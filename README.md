# \<abas-database-picker\>

Pick an abas database

## Example usage

```html
<link rel="import" href="../polymer/polymer-element.html">
<link rel="import" href="../aba-widget/aba-widget.html">
<link rel="import" href="../abas-widget-behavior/abas-widget-behavior.html">
<link rel="import" href="../abas-styles/abas-styles.html">
<link rel="import" href="../abas-config/abas-config-meta.html">
<link rel="import" href="../abas-database-picker/abas-database-picker.html">
<link rel="import" href="../../aba-js-imports/lodash-import.html">

<dom-module id="dosbox-widget">
    <template>
        <abas-config-meta data="{{configuration}}"></abas-config-meta>
        <custom-style></custom-style>
        <aba-widget>
            <div slot="content">
                <!-- Widget content goes here! -->
                <abas-database-picker company="erp" target-component="[[self]]"></abas-database-picker>
                <abas-database-picker company="erp" selection="{{selectedDatabase}}"></abas-database-picker>
            </div>
            <div slot="settingsForm">
                <!-- Widget settings goes here! -->
            </div>
            <div slot="optionsMenu">
                <!-- Additonal settings menu items -->
            </div>
        </aba-widget>
    </template>
    
    <script>
        class DosboxWidget extends ABAS.WidgetBehavior(Polymer.Element) {
            constructor () {
                super()
                this.self = this;
            }
            static get is() { return 'dosbox-widget' }
            static get properties() {
                return {
                    selectedDatabase: {
                        type: Object,
                        observer: "_databaseSelected"
                    }
                };
            }
            connectedCallback() {
                super.connectedCallback();
                this.loadResources(this.resolveUrl(this.importPath + 'locales.json'));
                this.addEventListener('abas-database-picker.database-selected', (event) => {
                    console.log(`Notification via event. Selected db is ${JSON.stringify(event.detail)}`)
                });
            }

            _databaseSelected(_change) {
                console.log(`Notification via property binding. Another db selected ${JSON.stringify(this.selectedDatabase)}`)
            }
            
        }
        customElements.define(DosboxWidget.is, DosboxWidget);
    </script>
</dom-module>
```

